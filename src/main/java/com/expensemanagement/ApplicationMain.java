package com.expensemanagement;

import com.expensemanagement.login.Login;
import com.expensemanagement.stagemaster.StageMaster;

import javafx.application.Application;
import javafx.stage.Stage;

public class ApplicationMain extends Application {

	public static void main(String args[]) {
		launch(args);
		
	}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
      new  Login().show();
	   
	   }
	
		
	}


