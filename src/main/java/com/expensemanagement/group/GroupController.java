package com.expensemanagement.group;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.expensemanagement.login.Login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class GroupController {

	@FXML
	private TextField groupName;
	
	@FXML
	private TextField groupType;
	@FXML
	private Button create;
	@FXML
	private Button back;
	
	public void create(ActionEvent event) throws IOException {
		if (groupName.getText().isEmpty() || groupType.getText().isEmpty() ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}

		  String reqBody = "{\n" + "\"groupName\"" + ":\"" + groupName.getText() + "\", \r\n" + "\"groupType\"" + ":\""
					+ groupType.getText() +  "\" \r\n"+ "\n}";


		System.out.println(groupName.getText());
		System.out.println(groupType.getText());
	

		String api = "http://localhost:8080/groups/api/v1/create";
		URL url = new URL(api);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);

		OutputStream osObj = con.getOutputStream();
		osObj.write(reqBody.getBytes());


		System.out.println(con.getResponseCode());
		if (con.getResponseCode() == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader irObj = new InputStreamReader(con.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
				System.out.println(sb.toString());
			}
			
			br.close();
			con.disconnect();
			
		}
	}
	
	public void back(ActionEvent event) {
		new Login().show();
	}
}
