package com.expensemanagement.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.stereotype.Controller;

import com.expensemanagement.group.CreateGroup;
import com.expensemanagement.signup.SignUp;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

@Controller
public class LoginController {

	@FXML
	private TextField email;

	@FXML
	private TextField mobile;

	@FXML
	private TextField password;

	@FXML
	private Button login;

	@FXML
	private Button signUp;

	public void login(ActionEvent event) throws IOException {

		if (email.getText().isEmpty() || mobile.getText().isEmpty() || password.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}

		  String reqBody = "{\n" + "\"email\"" + ":\"" + email.getText() + "\", \r\n" + "\"mobile\"" + ":\""
				+ mobile.getText() + "\", \r\n" + "\"password\"" + ":\"" + password.getText() + "\" \r\n" + "\n}";

		System.out.println(email.getText());
		System.out.println(mobile.getText());
		System.out.println(password.getText());

		String api = "http://localhost:8080/directory/api/v1/validate";
		URL url = new URL(api);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);

		OutputStream osObj = con.getOutputStream();
		osObj.write(reqBody.getBytes());


		System.out.println(con.getResponseCode());
		if (con.getResponseCode() == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader irObj = new InputStreamReader(con.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
				System.out.println(sb.toString());
				new CreateGroup().show();
			}
			
			br.close();
			con.disconnect();
			
		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("409");
			alert.setContentText("Enter valid Credentials.");
		System.out.println(con.getResponseCode());
		}

	}

	public void signUp(ActionEvent event) {
		new SignUp().show();
	}

}
