package com.expensemanagement.primaryscreen;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.expensemanagement.stagemaster.StageMaster;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

@Component
public class PrimaryScreen {
	public void show() {
		try{
			Parent actorGroup =FXMLLoader.load(getClass().getResource(getClass().getSimpleName()+".fxml"));
			StageMaster.getStage().setScene(new Scene(actorGroup));
			StageMaster.getStage().show();
		}catch(IOException e) {
			e.printStackTrace();
		}
}
}
