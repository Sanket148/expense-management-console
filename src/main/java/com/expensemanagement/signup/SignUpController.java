package com.expensemanagement.signup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.expensemanagement.login.Login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class SignUpController {

	@FXML
	private TextField name;

	@FXML
	private TextField mobile;

	@FXML
	private TextField email;

	@FXML
	private TextField password;

	@FXML
	private TextField country;

	@FXML
	private TextField currency;

	@FXML
	private TextField language;

	@FXML
	private Button SignUp;

	@FXML
	private Button back;

	public void SignUp(ActionEvent event) throws IOException {
		if (name.getText().isEmpty() || mobile.getText().isEmpty() || email.getText().isEmpty()
				|| password.getText().isEmpty() || country.getText().isEmpty() || currency.getText().isEmpty()
				|| language.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}

		String reqBody = "{\n" + "\"fullName\"" + ":\"" + name.getText() + "\", \r\n" + "\"email\"" + ":\""
				+ email.getText() + "\", \r\n" + "\"mobile\"" + ":\"" + mobile.getText() + "\", \r\n" + "\"password\""
				+ ":\"" + password.getText() + "\", \r\n" + "\"country\"" + ":\"" + country.getText() + "\", \r\n"
				+ "\"currency\"" + ":\"" + currency.getText() + "\", \r\n" + "\"language\"" + ":\"" + language.getText()
				+ "\" \r\n" + "\n}";
		
		
		System.out.println(name.getText());
		System.out.println(email.getText());
		System.out.println(mobile.getText());
		System.out.println(password.getText());
		System.out.println(country.getText());
		System.out.println(currency.getText());
		System.out.println(language.getText());



		String api = "http://localhost:8080/directory/api/v1/signup";
		URL url = new URL(api);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);

		OutputStream osObj = con.getOutputStream();
		osObj.write(reqBody.getBytes());

		System.out.println(con.getResponseCode());
		if (con.getResponseCode() == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader irObj = new InputStreamReader(con.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
				System.out.println(sb.toString());
				new Login().show();
			}

			br.close();
			con.disconnect();

		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("409");
			alert.setContentText("user already exist.");
			System.out.println(con.getResponseCode());
		}

	}

	public void back(ActionEvent event) {
		new Login().show();
	}

}
